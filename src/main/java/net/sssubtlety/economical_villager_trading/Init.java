package net.sssubtlety.economical_villager_trading;

import net.sssubtlety.economical_villager_trading.config.ConfigManager;

import net.fabricmc.api.ModInitializer;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ConfigManager.init();
    }
}
