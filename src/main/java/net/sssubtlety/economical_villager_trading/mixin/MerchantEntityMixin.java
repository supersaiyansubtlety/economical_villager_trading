package net.sssubtlety.economical_villager_trading.mixin;

import net.minecraft.entity.passive.MerchantEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.village.Merchant;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.TradeOfferList;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MerchantEntity.class)
abstract class MerchantEntityMixin extends PassiveEntity implements Merchant {
    @Shadow
    public abstract TradeOfferList getOffers();

    @Shadow
    @Nullable
    public abstract PlayerEntity getCurrentCustomer();

    @Shadow
    public abstract boolean isLeveledMerchant();

    protected MerchantEntityMixin() {
        // noinspection ConstantConditions
        super(null, null);
        throw new IllegalStateException("MerchantEntityMixin's dummy constructor called!");
    }

    // handler stub to be overriden in VillagerEntityMixin
    @Inject(method = "trade", at = @At("TAIL"))
    protected void tryRefreshPostTrade(TradeOffer offer, CallbackInfo ci) { }
}
