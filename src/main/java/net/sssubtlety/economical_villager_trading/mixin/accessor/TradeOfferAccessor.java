package net.sssubtlety.economical_villager_trading.mixin.accessor;

import net.minecraft.village.TradeOffer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(TradeOffer.class)
public interface TradeOfferAccessor {
    @Accessor("demandBonus")
    void economical_villager_trading$setDemandBonus(int bonus);
}
