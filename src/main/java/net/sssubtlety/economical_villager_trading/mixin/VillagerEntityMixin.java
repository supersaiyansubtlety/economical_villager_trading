package net.sssubtlety.economical_villager_trading.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import it.unimi.dsi.fastutil.objects.Object2DoubleArrayMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import net.fabricmc.fabric.api.tag.convention.v2.TagUtil;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.InteractionObserver;
import net.minecraft.entity.ai.brain.MemoryModuleType;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.screen.MerchantScreenHandler;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.VillagerData;
import net.minecraft.village.VillagerDataContainer;
import net.minecraft.village.VillagerType;
import net.minecraft.world.World;
import net.sssubtlety.economical_villager_trading.EconomicalVillagerTrading;
import net.sssubtlety.economical_villager_trading.config.ConfigManager;
import net.sssubtlety.economical_villager_trading.mixin.accessor.TradeOfferAccessor;

import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static java.lang.Boolean.TRUE;

@Mixin(VillagerEntity.class)
abstract class VillagerEntityMixin extends MerchantEntityMixin implements InteractionObserver, VillagerDataContainer {
	@Unique
	private static boolean isBuyTrade(TradeOffer tradeOffer) {
		return TagUtil.isIn(EconomicalVillagerTrading.BUY_TRADE_CURRENCIES, tradeOffer.getSellItem().getItem());
	}

	@Unique
	private static boolean shouldRestock(
		TradeOffer tradeOffer, ThreadLocal<Boolean> restockingAll, ThreadLocal<Boolean> restockingBuys
	) {
		return TRUE.equals(restockingAll.get()) ||
			// refreshType matches tradeType
			(TRUE.equals(restockingBuys.get()) == isBuyTrade(tradeOffer));
	}

	// these are "parameters" passed to `restock` and `updateDemandBonus`
	@Unique private ThreadLocal<Boolean> restockingBuys;
	@Unique private ThreadLocal<Boolean> restockingAll;

	@SuppressWarnings("FieldMayBeFinal")
	@Shadow private long lastRestockTime;

	@Shadow public abstract VillagerData getVillagerData();
	@Shadow public abstract int getExperience();
	@Shadow public abstract boolean canRefreshTrades();
	@Shadow public abstract void restock();

	// overrides handler stub in MerchantEntityMixin
	@Override
	protected void tryRefreshPostTrade(TradeOffer offer, CallbackInfo ci) {
		if (offer.isDisabled()) {
			this.restockingBuys.set(!isBuyTrade(offer));
			this.restock();
		}
	}

	@Inject(
		method = "<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;" +
			"Lnet/minecraft/village/VillagerType;)V",
		at = @At("TAIL")
	)
	private void initFields(
		EntityType<? extends VillagerEntity> entityType, World world, VillagerType type, CallbackInfo ci
	) {
		this.restockingBuys = new ThreadLocal<>();
		this.restockingAll = new ThreadLocal<>();
	}

	@Inject(method = "restock", at = @At("HEAD"))
	private void setAllTradesDisabled(
		CallbackInfo ci,
		@Share("allTradesDisabled") LocalBooleanRef allTradesDisabled
	) {
		allTradesDisabled.set(this.areAllTradesDisabled());
	}

	@WrapOperation(
		method = "restock",
		at = @At(value = "INVOKE", target = "Lnet/minecraft/village/TradeOffer;resetUses()V")
	)
	private void resetUsesIfShouldRestock(
		TradeOffer tradeOffer, Operation<Void> resetUses,
		@Share("allTradesDisabled") LocalBooleanRef allTradesDisabled
	) {
		if (
			tradeOffer.isDisabled() &&
			this.hasWorkStation() &&
			(allTradesDisabled.get() || shouldRestock(tradeOffer, this.restockingAll, this.restockingBuys))
		) {
			resetUses.call(tradeOffer);
		}
	}

	@Inject(method = "restock", at = @At(value = "TAIL"))
	private void updateGuiAndRemoveThreadLocalsAfterRestock(CallbackInfo ci) {
		this.restockingAll.remove();
		this.restockingBuys.remove();
        this.updateTradeGui();
	}

	// inject after lastRestockCheckTime:
	// - after lastRestockCheckTime is set
	// - before doDailyRestock() is called, which resets all trade uses
	@Inject(method = "tryDailyRestock", cancellable = true, at = @At(
		value = "FIELD", opcode = Opcodes.PUTFIELD, shift = At.Shift.AFTER,
		target = "Lnet/minecraft/entity/passive/VillagerEntity;lastRestockCheckTime:J"
	))
	private void shouldNotRestockUnlessAllTradesDisabled(CallbackInfoReturnable<Boolean> cir) {
		if (this.getWorld().getTime() > this.lastRestockTime + 12000L && this.areAllTradesDisabled()) {
			// set restockingAll for updateDemandBonus (in restockAndUpdateDemandBonus in doDailyRestock)
			this.restockingAll.set(true);
		} else {
            cir.setReturnValue(false);
        }
	}

	 // Remove `restockingAll` after `VillagerEntity#doDailyRestock`, because if `doDailyRestock` is
	 // reached, `shouldNotRestockUnlessAllTradesDisabled` set `refreshingAll`.
	 // `tryDailyRestock` will return `false` and prevent `restock` from being called in this case,
	 // so this won't prevent `restock` from getting `refreshingAll`.
	@Inject(
		method = "tryDailyRestock",
		at = @At(
			shift = At.Shift.AFTER,
			value = "INVOKE",
			target = "Lnet/minecraft/entity/passive/VillagerEntity;doDailyRestock()V"
		)
	)
	private void removeRefreshingAll(CallbackInfoReturnable<Boolean> cir) {
		this.restockingAll.remove();
	}

	@Inject(method = "levelUp", at = @At("RETURN"))
	private void refreshAllPostLevelUp(CallbackInfo info) {
		this.restockingAll.set(true);
		this.restock();
        if (ConfigManager.villagerLevelingWithGuiOpenEnabled()) {
            this.updateTradeGui();
		}
	}

	@Inject(method = "updateDemandBonus", at = @At("HEAD"), cancellable = true)
	private void balancePriceAdjustments(CallbackInfo ci) {
		final double dilutionFactor = ConfigManager.getVillagerTradeAdjustBalance();

		// dilutionFactor < 0 means use vanilla bonuses
		if (dilutionFactor >= 0) {
			if (dilutionFactor >= 100) {
				// dilutionFactor >= 100 means completely disable demandBonus
				for (final TradeOffer offer : this.getOffers()) {
					((TradeOfferAccessor) offer).economical_villager_trading$setDemandBonus(0);
				}
			} else {
				// Replace vanilla demandBonus adjustment with one that makes a trade more expensive the more
				// disproportionately it was used relative to other trades.
				// The greater the value of dilutionFactor, the less the difference between
				// the most- and least-used trades is.
				final var useRatios = new Object2DoubleArrayMap<TradeOffer>();
				final var offers = this.getOffers();
				final boolean allTradesDisabled = this.areAllTradesDisabled();

				double useRatioSum = 0;
				for (final var offer : offers) {
					if (allTradesDisabled || shouldRestock(offer, this.restockingAll, this.restockingBuys)) {
						final double useRatio = ((double) offer.getUses()) / offer.getMaxUses();
						useRatioSum += useRatio;
						useRatios.put(offer, useRatio);
					}
				}

				final double averageUseRatio = useRatioSum / offers.size();

				for (final Object2DoubleMap.Entry<TradeOffer> entry : useRatios.object2DoubleEntrySet()) {
					final TradeOffer offer = entry.getKey();
					((TradeOfferAccessor) offer).economical_villager_trading$setDemandBonus(
                        this.getAdjustedBonus(averageUseRatio, offer, entry.getDoubleValue(), dilutionFactor)
					);
				}
			}

			// replace vanilla behavior if dilutionFactor >= 0
			ci.cancel();
		}
	}

	@ModifyArg(method = "afterUsing", at = @At(
		value = "INVOKE",
		target = "Lnet/minecraft/entity/ExperienceOrbEntity;<init>(Lnet/minecraft/world/World;DDDI)V"
	))
	private int multiplyXp(int originalValue) {
		return (int)(ConfigManager.getVillagerXpMultiplier() * originalValue);
	}

	@ModifyExpressionValue(
		method = "mobTick",
		allow = 1, require = 1,
		slice = @Slice(to = @At(value = "INVOKE", target = "Lnet/minecraft/entity/passive/VillagerEntity;levelUp()V")),
		at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/passive/VillagerEntity;hasCustomer()Z")
	)
	private boolean allowLevelingWithCustomerIfEnabled(boolean hasCustomer) {
		return hasCustomer && !ConfigManager.villagerLevelingWithGuiOpenEnabled();
	}

	@Unique
	private void updateTradeGui() {
		if (
			this.getCurrentCustomer() != null &&
			this.getCurrentCustomer().currentScreenHandler instanceof MerchantScreenHandler
		) {
			this.getCurrentCustomer().sendTradeOffers(
				this.getCurrentCustomer().currentScreenHandler.syncId, this.getOffers(),
				this.getVillagerData().getLevel(), this.getExperience(),
				this.isLeveledMerchant(), this.canRefreshTrades()
			);
		}
	}

	@Unique
	private int getAdjustedBonus(double avgUseRatio, TradeOffer offer, double useRatio, double dilutionFactor) {
		return Math.round(Math.round(
			((double) offer.getDemandBonus()) * ((useRatio + dilutionFactor) / (avgUseRatio + dilutionFactor))
		));
	}

	@Unique
	private boolean areAllTradesDisabled() {
		for (final TradeOffer offer : this.getOffers()) {
			if (!offer.isDisabled()) {
				return false;
			}
		}

		return true;
	}

	@Unique
	private boolean hasWorkStation() {
		return this.getBrain().getOptionalMemory(MemoryModuleType.JOB_SITE).isPresent();
	}
}
