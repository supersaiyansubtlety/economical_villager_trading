package net.sssubtlety.economical_villager_trading.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import net.minecraft.village.TradeOffer;

import net.sssubtlety.economical_villager_trading.config.ConfigManager;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(TradeOffer.class)
abstract class TradeOfferMixin {
    @ModifyExpressionValue(
        method = "getAdjustedPrice",
        at = @At(value = "FIELD", target = "Lnet/minecraft/village/TradeOffer;specialPrice:I")
    )
    private int return0IfSpecialPriceDisabled(int specialPrice) {
        return ConfigManager.specialDiscountsEnabled() ? specialPrice : 0;
    }
}
