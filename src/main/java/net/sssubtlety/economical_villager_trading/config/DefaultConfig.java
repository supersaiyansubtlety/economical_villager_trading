package net.sssubtlety.economical_villager_trading.config;

import net.sssubtlety.economical_villager_trading.EconomicalVillagerTrading;
import net.sssubtlety.economical_villager_trading.Util;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.function.Function;

import static net.sssubtlety.economical_villager_trading.EconomicalVillagerTrading.NAMESPACE;

public final class DefaultConfig implements Config {
    public static final DefaultConfig INSTANCE = new DefaultConfig();

    private DefaultConfig() { }

    @Override
    public double villagerTradeAdjustBalance() {
        return -1;
    }

    @Override
    public boolean enableReputationPriceAdjustments() { return
        true;
    }

    @Override
    public double villagerXpMultiplier() {
        return 1;
    }

    @Override
    public boolean enableVillagerLevelupWhileTrading() { return true; }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return NoConfigScreen::new;
    }

    public static final class NoConfigScreen extends Screen {
        // WHITE's color is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int TITLE_COLOR = Formatting.WHITE.getColorValue();
        // RED's color is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int MESSAGE_COLOR = Formatting.RED.getColorValue();
        private static final Text NO_CONFIG_SCREEN_TITLE = Text.translatable("text." + NAMESPACE + ".no_config_screen.title");
        private static final Text NO_CONFIG_SCREEN_MESSAGE = Text.translatable("text." + NAMESPACE + ".no_config_screen.message");

        private final Screen parent;
        private NoConfigScreen(Screen parent) {
            super(NO_CONFIG_SCREEN_TITLE);
            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int horizontalCenter = this.width / 2;

            graphics.drawCenteredShadowedText(
                this.textRenderer,
                Util.replace(NO_CONFIG_SCREEN_TITLE, "\\$\\{name\\}", EconomicalVillagerTrading.NAME.getString()),
                horizontalCenter, this.height / 10,
                TITLE_COLOR
            );

            graphics.drawCenteredShadowedText(
                this.textRenderer,
                NO_CONFIG_SCREEN_MESSAGE,
                horizontalCenter, this.height / 2,
                MESSAGE_COLOR
            );
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public void closeScreen() {
            this.client.setScreen(this.parent);
        }
    }
}
