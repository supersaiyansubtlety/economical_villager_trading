package net.sssubtlety.economical_villager_trading.config;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

import static net.sssubtlety.economical_villager_trading.EconomicalVillagerTrading.LOGGER;
import static net.sssubtlety.economical_villager_trading.EconomicalVillagerTrading.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class ClothConfig implements ConfigData, Config {
    @ConfigEntry.Gui.Tooltip
    public double villagerTradeAdjustBalance = DefaultConfig.INSTANCE.villagerTradeAdjustBalance();
    @ConfigEntry.Gui.Tooltip
    public boolean enableReputationPriceAdjustments = DefaultConfig.INSTANCE.enableReputationPriceAdjustments();
    @ConfigEntry.Gui.Tooltip
    public double villagerXpMultiplier = DefaultConfig.INSTANCE.villagerXpMultiplier();
    @ConfigEntry.Gui.Tooltip
    public boolean enableVillagerLevelupWhileTrading = DefaultConfig.INSTANCE.enableVillagerLevelupWhileTrading();

    static ClothConfig register() {
        return AutoConfig.register(ClothConfig.class, GsonConfigSerializer::new).getConfig();
    }

    @Override
    public void validatePostLoad() {
        if (this.villagerTradeAdjustBalance < -1) {
            this.villagerTradeAdjustBalance = -1;
        } else if (this.villagerTradeAdjustBalance > 100) {
            this.villagerTradeAdjustBalance = 100;
        }

        if (this.villagerXpMultiplier < 0) {
            LOGGER.warn("XP multiplier can't be less than 0.");

            this.villagerXpMultiplier = 0;
        }

        if (this.villagerXpMultiplier > 2) {
            LOGGER.warn("XP multiplier can't be grater than 2.");

            this.villagerXpMultiplier = 2;
        }
    }

    @Override
    public double villagerTradeAdjustBalance() {
        return this.villagerTradeAdjustBalance;
    }

    @Override
    public boolean enableReputationPriceAdjustments() {
        return this.enableReputationPriceAdjustments;
    }

    @Override
    public double villagerXpMultiplier() {
        return this.villagerXpMultiplier;
    }

    @Override
    public boolean enableVillagerLevelupWhileTrading() {
        return this.enableVillagerLevelupWhileTrading;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return parent -> AutoConfig.getConfigScreen(ClothConfig.class, parent).get();
    }
}
