package net.sssubtlety.economical_villager_trading.config;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

import static net.sssubtlety.economical_villager_trading.Util.isModLoaded;

public class ConfigManager {
    private static final Config CONFIG = isModLoaded("cloth-config", ">=6.1.48") ?
        ClothConfig.register() : DefaultConfig.INSTANCE;

    public static void init() { }

    public static double getVillagerTradeAdjustBalance() {
        return CONFIG.villagerTradeAdjustBalance();
    }

    public static boolean specialDiscountsEnabled() {
        return CONFIG.enableReputationPriceAdjustments();
    }

    public static double getVillagerXpMultiplier() {
        return CONFIG.villagerXpMultiplier();
    }

    public static boolean villagerLevelingWithGuiOpenEnabled() {
        return CONFIG.enableVillagerLevelupWhileTrading();
    }

    public static Function<Screen, ? extends Screen> getScreenFactory() {
        return CONFIG.screenFactory();
    }
}
