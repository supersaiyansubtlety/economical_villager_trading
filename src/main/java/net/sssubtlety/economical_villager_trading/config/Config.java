package net.sssubtlety.economical_villager_trading.config;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

public interface Config {
    double villagerTradeAdjustBalance();

    boolean enableReputationPriceAdjustments();

    double villagerXpMultiplier();

    boolean enableVillagerLevelupWhileTrading();

    Function<Screen, ? extends Screen> screenFactory();
}
