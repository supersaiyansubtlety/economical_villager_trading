package net.sssubtlety.economical_villager_trading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.item.Item;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public final class EconomicalVillagerTrading {
    private EconomicalVillagerTrading() { }

    public static final String NAMESPACE = "economical_villager_trading";
    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
    public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);

    public static final TagKey<Item> BUY_TRADE_CURRENCIES =
        TagKey.of(RegistryKeys.ITEM, Identifier.of(NAMESPACE, "buy_trade_currencies"));
}
